Embedded project for an STM32 Cortex M3 to operate a small seed-starting greenhouse! I decided to write this project with Zig because I think it is a great fit for embedded development.

The main goal of the project is to keep the seedling sprouts from dying. This is how:

- Heater for days and nights that are too cold outside
- Servo-controlled vent(s) that opens and closes for fresh air and cooling when the sun heats too much

![greenhouse](https://lh3.googleusercontent.com/pw/AM-JKLV9THimKBDE2o-mZOKUygB-QxOU6vU5WX_CwgEVmXemElyqMxfVpVA90Mn4u4kwPN76-3SihN6QZT9x-0PZAL37usCZzmDIDXgM5YsQ4vDKenCOUG0iNuZUnOIHo4D7XJ3lVn7JmoebFMZoKwo4P4-DpQ=h600)

## Links

- [Bluepill Pinout](https://raw.githubusercontent.com/stm32-rs/stm32f1xx-hal/f9b24f4d9bac7fc3c93764bd295125800944f53b/BluePillPinout.jpg)
- [Reference Manual (12.5MB PDF download)](https://www.st.com/resource/en/reference_manual/cd00171190-stm32f101xx-stm32f102xx-stm32f103xx-stm32f105xx-and-stm32f107xx-advanced-arm-based-32-bit-mcus-stmicroelectronics.pdf)

## Misc notes while developing:

svd downloaded from https://raw.githubusercontent.com/posborne/cmsis-svd/master/data/STMicro/STM32F103xx.svd
Used [svd4zig](https://github.com/rbino/svd4zig) to convert

build and flash to device (requires stlink utilities installed):
`zig build flash`

start an on-chip debugger session which gdb can connect to:
`openocd -f interface/stlink-v2.cfg -f target/stm32f1x.cfg`

connect with gdb (replace .elf name if binary changes):
`gdb-multiarch zig-out/bin/greenhouse.elf -ex "target remote :3333"`

Check out size of program:
`arm-none-eabi-size zig-out/bin/zig-stm32-blink.elf`

gdb helpful commands:

- `ctrl-c` stop execution where it's at
- `c` or `continue` continue execution
- `load` reload elf file to device for code updates
- `print <variable-name>` prints variable value
- 

other notes:

- When flashing blue pill, make sure to have blue pill getting power through usb. The stlink is not enough and leads to weird flashing issues.
