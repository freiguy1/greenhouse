const std = @import("std");
const regs = @import("regs.zig");
const delay_us = @import("delay.zig").delay_us;
const delay_ms = @import("delay.zig").delay_ms;
const read_dht22 = @import("dht22.zig").read;
const Servo = @import("Servo.zig");
const rf = @import("rf.zig");
const packet = @import("packet.zig");
const Packet = packet.Packet;
const consts = @import("consts.zig");

pub const FREQ = 72_000_000;
// pub const FREQ = 32_000_000;
const low_temp: f32 = 68;
const hyst: f32 = 2;
const heater_on_if_below = low_temp - hyst;
const heater_off_if_above = low_temp + hyst;

var led_state: u1 = 0;

pub fn main() void {
    // GPIO SETUP
    regs.RCC.APB2ENR.modify(.{ .IOPAEN = 1 }); // enable GPIOA clk
    regs.GPIOA.CRH.modify(.{ .MODE10 = 0b10, .CNF10 = 0b00 }); // PA10 = output 2MHz (heater)
    regs.GPIOA.CRH.modify(.{ .MODE9 = 0b00, .CNF9 = 0b10 }); // PA9 = Input with pull up/down (dht22)
    regs.GPIOA.ODR.modify(.{ .ODR9 = 0b1 }); // PA9 input pull up

    var servo = Servo.init(0);
    var servo_setting: u8 = 0;
    setHeater(false);
    rf.init_pins();

    // Allow stuff to start up (looking at you dht22)
    delay_ms(2 * std.time.ms_per_s);

    rf.reset_and_init_settings() catch {};

    while (true) {
        defer delay_ms(30 * std.time.ms_per_s);

        const dht22_data = if (read_dht22()) |val| blk: {
            break :blk val;
        } else |_| {
            setHeater(false);
            continue;
        };

        const temp = dht22_data.temp;
        const humidity = dht22_data.humidity;

        if (temp <= heater_on_if_below) {
            setHeater(true);
        } else if (temp >= heater_off_if_above) {
            setHeater(false);
        }

        if (servo_setting == 0) {
            servo.setValue(0);
        } else if (servo_setting == 1) {
            servo.setValue(63);
        } else if (servo_setting == 2) {
            servo.setValue(127);
        } else if (servo_setting == 3) {
            servo.setValue(191);
        } else if (servo_setting == 4) {
            servo.setValue(255);
        }

        servo_setting = (servo_setting + 1) % 5;

        // Send temp data
        var temp_p = createAdafruitPacket(temp, "temperature");
        const temp_p_bytes = temp_p.toBytes();
        rf.send(64, temp_p_bytes) catch {
            // @breakpoint();
        };

        // Send humidity data
        var humidity_p = createAdafruitPacket(humidity, "humidity");
        const humidity_p_bytes = humidity_p.toBytes();
        rf.send(64, humidity_p_bytes) catch {
            // @breakpoint();
        };
    }
}

fn createAdafruitPacket(value: f32, feed_name: []const u8) Packet {
    return Packet{
        .to_addr = consts.edge_addr,
        .from_addr = consts.my_addr,
        .op = packet.Operation{ .UploadToAdafruit = packet.AdafruitBundle{
            .value = value,
            .feed_name = feed_name,
        } },
    };
}

fn setHeater(state: bool) void {
    const bit: u1 = if (state) 1 else 0;
    regs.GPIOA.ODR.modify(.{ .ODR10 = bit });
}
