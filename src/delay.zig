const Register = @import("regs.zig").Register;
const FREQ = @import("main.zig").FREQ;

pub fn delay_us(us: u32) void {
    var ticks = @intCast(u64, us) * @intCast(u64, FREQ) / 1_000_000;

    // Set clock source to core (not external)
    SYST.CSR.modify(.{ .CLKSOURCE = 1 });

    var full_cycles = ticks >> 24;
    if (full_cycles > 0) {
        // Set reload value
        SYST.RVR.write(.{ .RELOAD = 0xFFFFFF });
        // Clear current counter
        SYST.CVR.write_raw(0);
        // Enable counter
        SYST.CSR.modify(.{ .ENABLE = 1 });
        while (full_cycles > 0) : (full_cycles -= 1) {
            while (SYST.CSR.read().COUNTFLAG != 1) {}
        }
    }

    ticks = ticks & 0xFFFFFF;
    if (ticks > 1) {
        // Set reload value
        SYST.RVR.write(.{ .RELOAD = @truncate(u24, ticks) });
        // Clear current counter
        SYST.CVR.write_raw(0);
        // Enable counter
        SYST.CSR.modify(.{ .ENABLE = 1 });

        while (SYST.CSR.read().COUNTFLAG != 1) {}
    }

    // Disable SysTick
    SYST.CSR.write(.{ .ENABLE = 0 });
}

pub fn delay_ms(ms: u32) void {
    var ms_left = ms;
    // 4294967 is the highest u32 value which you can multiply by 1000 without overflow
    while (ms_left > 4294967) : (ms_left -= 4294967) {
        delay_us(4294967000);
    }
    delay_us(ms_left * 1_000);
}

/// SysTick system timer
const SYST = struct {
    const base_address = 0xE000E010;
    /// CSR
    const CSR_val = packed struct {
        /// ENABLE [0:0]
        /// ENABLE
        ENABLE: u1 = 0,
        /// TICKINT [1:1]
        /// TICKINT
        TICKINT: u1 = 0,
        /// CLKSOURCE [2:2]
        /// CLKSOURCE
        CLKSOURCE: u1 = 0,
        /// unused [3:15]
        _unused3: u13 = 0,
        /// COUNTFLAG [16:16]
        /// COUNTFLAG
        COUNTFLAG: u1 = 1,
        /// unused [17:31]
        _unused17: u15 = 0,
    };
    /// Controls the system timer and provides status data.
    pub const CSR = Register(CSR_val).init(base_address + 0x0);

    /// RVR
    const RVR_val = packed struct {
        /// RELOAD [0:23]
        /// RELOAD
        RELOAD: u24 = 0,
        /// unused [24:31]
        _unused24: u8 = 0,
    };
    /// Holds the reload value of the SYST_CVR.
    pub const RVR = Register(RVR_val).init(base_address + 0x4);

    /// CVR
    const CVR_val = packed struct {
        /// CURRENT [0:31]
        /// CURRENT
        CURRENT: u32 = 0,
    };
    /// Reads or clears the current counter value
    pub const CVR = Register(CVR_val).init(base_address + 0x8);
};
