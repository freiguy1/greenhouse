const Self = @This();
const std = @import("std");
const FREQ = @import("main.zig").FREQ;
const regs = @import("regs.zig");

const PRESCALER = FREQ / (0xFFFF * 50) + 2; // 72MHz / (max count of timer * 50 hz required servo freq) +2 gives us more wiggle room for calibration
const TIMER_FREQ = FREQ / PRESCALER;
const AUTO_RELOAD_ADJUST = -2600; // lower number, faster Hz
const AUTO_RELOAD = TIMER_FREQ / 50 + AUTO_RELOAD_ADJUST; // 50Hz is required servo frequency
const MIN_DUTY = AUTO_RELOAD / 40; // Period is 20ms, min duty is 0.5 ms. So divide period by (20 / .5)
const MAX_DUTY = AUTO_RELOAD / 8; // Period is 20ms, max duty is 2.5 ms. So divide period by (20 / 2.5)

value: u8 = 0,

pub fn init(value: u8) Self {
    // Enable system clock to TIM2
    regs.RCC.APB1ENR.modify(.{ .TIM2EN = 1 }); // enable TIM2
    // Set PA0 to alternate function push pull output 2MHz
    regs.GPIOA.CRL.modify(.{ .MODE0 = 0b10, .CNF0 = 0b10 });

    // Set prescaler
    regs.TIM2.PSC.write_raw(PRESCALER);
    // Set auto reload register
    regs.TIM2.ARR.write_raw(AUTO_RELOAD); // 2.25MHz / 45,000 = 50Hz period
    // Set duty cycle
    regs.TIM2.CCR1.write_raw(valueToDutyCycle(value));
    // Enable PWM mode 1, set preload
    regs.TIM2.CCMR1_Output.modify(.{ .OC1M = 0b110, .OC1PE = 1 });
    // Enable output 1 for TIM2
    regs.TIM2.CCER.modify(.{ .CC1E = 1 });
    // Set auto preload
    regs.TIM2.CR1.modify(.{ .ARPE = 1 });
    // Manually set interrupt to load all registers (or something)
    regs.TIM2.EGR.modify(.{ .UG = 1 });

    // Enable TIM2
    regs.TIM2.CR1.modify(.{ .CEN = 1 });

    return Self{ .value = value };
}

pub fn setValue(self: *Self, value: u8) void {
    self.value = value;
    regs.TIM2.CCR1.write_raw(valueToDutyCycle(value));
}

fn valueToDutyCycle(value: u8) u32 {
    return @intCast(u32, value) * (MAX_DUTY - MIN_DUTY) / std.math.maxInt(@TypeOf(value)) + MIN_DUTY;
}
