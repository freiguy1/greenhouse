const main = @import("main.zig");
const regs = @import("regs.zig");

// These symbols come from the linker script
extern const _data_loadaddr: u32;
extern var _data: u32;
extern const _edata: u32;
extern var _bss: u32;
extern const _ebss: u32;

export fn resetHandler() void {
    // Copy data from flash to RAM
    const data_loadaddr = @ptrCast([*]const u8, &_data_loadaddr);
    const data = @ptrCast([*]u8, &_data);
    const data_size = @ptrToInt(&_edata) - @ptrToInt(&_data);
    for (data_loadaddr[0..data_size]) |d, i| data[i] = d;

    // Clear the bss
    const bss = @ptrCast([*]u8, &_bss);
    const bss_size = @ptrToInt(&_ebss) - @ptrToInt(&_bss);
    for (bss[0..bss_size]) |*b| b.* = 0;

    systemInit();

    // Call contained in main.zig
    main.main();

    unreachable;
}

// copied verbatim from STM32 SDK
fn systemInit() void {
    //* Reset the RCC clock configuration to the default reset state(for debug purpose) */
    //* Set HSION bit */
    // regs.RCC.*.CR |= u32(0x00000001);
    regs.RCC.CR.modify(.{ .HSION = 1 });

    //* Reset SW, HPRE, PPRE1, PPRE2, ADCPRE and MCO bits */
    // regs.RCC.*.CFGR &= u32(0xF8FF0000);
    regs.RCC.CFGR.modify(.{ .SW = 0, .HPRE = 0, .PPRE1 = 0, .PPRE2 = 0, .ADCPRE = 0, .MCO = 0 });

    //* Reset HSEON, CSSON and PLLON bits */
    // regs.RCC.*.CR &= u32(0xFEF6FFFF);
    regs.RCC.CR.modify(.{ .HSEON = 0, .CSSON = 0, .PLLON = 0 });

    //* Reset HSEBYP bit */
    // regs.RCC.*.CR &= u32(0xFFFBFFFF);
    regs.RCC.CR.modify(.{ .HSEBYP = 0 });

    //* Reset PLLSRC, PLLXTPRE, PLLMUL and OTGFSPRE bits */
    // regs.RCC.*.CFGR &= u32(0xFF80FFFF);
    regs.RCC.CFGR.modify(.{ .PLLSRC = 0, .PLLXTPRE = 0, .PLLMUL = 0, .OTGFSPRE = 0 });

    //* Disable all interrupts and clear pending bits  */
    // regs.RCC.*.CIR = 0x009F0000;
    regs.RCC.CIR.write_raw(0x009F0000);

    //* Configure the System clock frequency, HCLK, PCLK2 and PCLK1 prescalers */
    //* Configure the Flash Latency cycles and enable prefetch buffer */
    setSysClock();

    // regs.SCB.*.VTOR = FLASH_BASE | VECT_TAB_OFFSET; //* Vector Table Relocation in Internal FLASH. */
}

fn setSysClock() void {
    const HSE_STARTUP_TIMEOUT: u16 = 0x0500;
    const RESET = 0;
    var startup_counter: u32 = 0;
    var hse_status: u1 = 0;

    //* SYSCLK, HCLK, PCLK2 and PCLK1 configuration ---------------------------*/
    //* Enable HSE */
    // regs.RCC.*.CR |= u32(regs.RCC_CR_HSEON);
    regs.RCC.CR.modify(.{ .HSEON = 1 });

    //* Wait till HSE is ready and if Time out is reached exit */
    hse_status = regs.RCC.CR.read().HSERDY;
    startup_counter += 1;
    while (hse_status == 0 and startup_counter != HSE_STARTUP_TIMEOUT) {
        hse_status = regs.RCC.CR.read().HSERDY;
        startup_counter += 1;
    }

    // if ((regs.RCC.*.CR & regs.RCC_CR_HSERDY) != regs.RESET) {
    if (regs.RCC.CR.read().HSERDY != RESET) {
        hse_status = 1;
    } else {
        hse_status = 0;
    }

    if (hse_status == 1) {
        //* Enable Prefetch Buffer */
        // regs.FLASH.*.ACR |= regs.FLASH_ACR_PRFTBE;
        // Is already enabled by default. Commenting
        // regs.FLASH.ACR.modify(.{ .PRFTBE = 1 });

        //* Flash 2 wait state */
        // regs.FLASH.*.ACR &= u32(~regs.FLASH_ACR_LATENCY);
        // regs.FLASH.*.ACR |= u32(regs.FLASH_ACR_LATENCY_2);
        regs.FLASH.ACR.modify(.{ .LATENCY = 0b010 });

        //* HCLK = SYSCLK */
        // regs.RCC.*.CFGR |= u32(regs.RCC_CFGR_HPRE_DIV1);
        regs.RCC.CFGR.modify(.{ .HPRE = 0 });

        //* PCLK2 = HCLK */
        // regs.RCC.*.CFGR |= u32(regs.RCC_CFGR_PPRE2_DIV1);
        regs.RCC.CFGR.modify(.{ .PPRE2 = 0 });

        //* PCLK1 = HCLK / 2 */
        // regs.RCC.*.CFGR |= u32(regs.RCC_CFGR_PPRE1_DIV2);
        regs.RCC.CFGR.modify(.{ .PPRE1 = 0b100 });

        //*  PLL configuration: PLLCLK = HSE * 9 = 72 MHz */
        // regs.RCC.*.CFGR &= u32(~u32(regs.RCC_CFGR_PLLSRC | regs.RCC_CFGR_PLLXTPRE | regs.RCC_CFGR_PLLMULL));
        // regs.RCC.*.CFGR |= u32(regs.RCC_CFGR_PLLSRC_HSE | regs.RCC_CFGR_PLLMULL9);
        regs.RCC.CFGR.modify(.{
            .PLLSRC = 1,
            .PLLXTPRE = 0,
            .PLLMUL = 0b0111, // * 9
            // .PLLMUL = 0b0010, // * 4
        });

        //* Enable PLL */
        // regs.RCC.*.CR |= regs.RCC_CR_PLLON;
        regs.RCC.CR.modify(.{ .PLLON = 1 });

        //* Wait till PLL is ready */
        // while ((regs.RCC.*.CR & regs.RCC_CR_PLLRDY) == 0) {}
        while (regs.RCC.CR.read().PLLRDY == 0) {}

        //* Select PLL as system clock source */
        // regs.RCC.*.CFGR &= u32(~u32(regs.RCC_CFGR_SW));
        // regs.RCC.*.CFGR |= u32(regs.RCC_CFGR_SW_PLL);
        regs.RCC.CFGR.modify(.{ .SW = 0b10 });

        //* Wait till PLL is used as system clock source */
        // while ((regs.RCC.*.CFGR & u32(regs.RCC_CFGR_SWS)) != u32(0x08)) {}
        while (regs.RCC.CFGR.read().SWS != 0b10) {}

        // Turn off internal osc
        regs.RCC.CR.modify(.{ .HSION = 0 });
    } else { //* If HSE fails to start-up, the application will have wrong clock
        //  configuration. User can add here some code to deal with this error */
        @breakpoint();
    }
}
