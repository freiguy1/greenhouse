const std = @import("std");

const PacketError = error{ParseError};

pub const OperationType = enum(u8) {
    UploadToAdafruit = 1,

    fn fromByte(byte: u8) PacketError!OperationType {
        return std.meta.intToEnum(OperationType, byte) catch error.ParseError;
    }
};

pub const Operation = union(OperationType) {
    UploadToAdafruit: AdafruitBundle,

    fn fromBytes(bytes: [62]u8) PacketError!Operation {
        const op_type = try OperationType.fromByte(bytes[0]);

        switch (op_type) {
            OperationType.UploadToAdafruit => return Operation{
                .UploadToAdafruit = AdafruitBundle{
                    .value = @bitCast(f32, bytes[2..6].*),
                    .feed_name = std.mem.sliceTo(bytes[6..], 0),
                },
            },
        }
    }

    fn toBytes(self: Operation, buf: *[61]u8) void {
        switch (self) {
            .UploadToAdafruit => |bundle| {
                const value_array = @bitCast([4]u8, bundle.value);
                std.mem.copy(u8, buf[1..], &value_array);
                std.mem.copy(u8, buf[5..], bundle.feed_name);
            },
        }
    }
};

pub const AdafruitBundle = struct {
    value: f32,
    feed_name: []const u8,
};

pub const Packet = struct {
    to_addr: u8,
    from_addr: u8,
    op: Operation,

    pub fn fromBytes(bytes: [64]u8) PacketError!Packet {
        return Packet{
            .to_addr = bytes[0],
            .from_addr = bytes[1],
            .op = try Operation.fromBytes(bytes[2..].*),
        };
    }

    pub fn toBytes(self: Packet) [64]u8 {
        var bytes = [_]u8{0} ** 64;
        bytes[0] = self.to_addr;
        bytes[1] = self.from_addr;
        bytes[2] = @enumToInt(self.op);
        self.op.toBytes(bytes[3..]);
        return bytes;
    }
};

const expect = std.testing.expect;

test "Packet.fromBytes" {
    var bytes = [_]u8{0} ** 64;
    bytes[0] = 10;
    bytes[1] = 13;
    bytes[2] = 1;
    bytes[3] = 0; // unused - data type for adafruit bundle. assuming u32
    std.mem.copy(u8, bytes[4..], &std.mem.toBytes(@as(f32, 32.345)));
    std.mem.copy(u8, bytes[8..], "greenhouse_temp");

    const p = try Packet.fromBytes(bytes);
    try expect(p.op.UploadToAdafruit.value == 32.345);
    try expect(std.mem.eql(u8, p.op.UploadToAdafruit.feed_name, "greenhouse_temp"));
}

test "Packet.toBytes" {
    const p = Packet{
        .to_addr = 10,
        .from_addr = 20,
        .op = Operation{ .UploadToAdafruit = AdafruitBundle{
            .value = 3.14159,
            .feed_name = "greenhouse_humidity",
        } },
    };
    const bytes = p.toBytes();
    try expect(bytes[0] == 10);
    try expect(bytes[1] == 20);
    try expect(bytes[2] == 1);
    try expect(bytes[3] == 0);
    const value_as_bytes = std.mem.toBytes(@as(f32, 3.14159));
    try expect(std.mem.eql(u8, bytes[4..8], &value_as_bytes));
    try expect(std.mem.eql(u8, bytes[8..27], "greenhouse_humidity"));
}
