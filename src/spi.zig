const std = @import("std");
const regs = @import("regs.zig");
const delay_us = @import("delay.zig").delay_us;

pub const SpiError = error{Timeout};

// Assumes GPIOA is turned on
pub fn init() void {
    // Set all GPIO (page 167)
    // Set SCK to alternate function output push-pull
    regs.GPIOA.CRL.modify(.{ .MODE5 = 0b11, .CNF5 = 0b10 });
    // Set MOSI to alternate function output push-pull
    regs.GPIOA.CRL.modify(.{ .MODE7 = 0b11, .CNF7 = 0b10 });
    // Set MISO Input floating (or pull-up if needed)
    regs.GPIOA.CRL.modify(.{ .MODE6 = 0b00, .CNF6 = 0b01 });
    // Set NSS alternate function output push-pull
    regs.GPIOA.CRL.modify(.{ .MODE4 = 0b11, .CNF4 = 0b10 });

    // Enable SPI peripheral
    regs.RCC.APB2ENR.modify(.{ .SPI1EN = 1 });

    // Configure SPI in master mode
    // MSB first, CPOL = 0, CPHA = 0 (default)
    // DFF to 8-bit data frame format (default)
    // Software slave select disabled (default)
    regs.SPI1.CR1.modify(.{
        // 10MHz max baud rate, set to 72MHz/16 = 4.5MHz
        .BR = 0b011,
        // Master mode enable
        .MSTR = 0b1,
    });
    // Slave select output enable
    regs.SPI1.CR2.modify(.{ .SSOE = 0b1 });
}

pub fn send(byte: u8) SpiError!void {
    // One byte at 4.5MHz takes 1.8us. Delaying 10 should be sufficient.
    // try withTimeout(10, 1, isTxEmpty);

    regs.SPI1.CR1.modify(.{ .SPE = 0b1 }); //enable spi
    defer regs.SPI1.CR1.modify(.{ .SPE = 0b0 }); //disable spi
    regs.SPI1.DR.write_raw(@intCast(u32, byte));
    try withTimeout(20, 5, isNotBusy);
}

// Note: send must be called first, yo!
// pub fn read() SpiError!u8 {
//     // One byte at 4.5MHz takes 1.8us. Delaying 10 should be sufficient.
//     try withTimeout(10, 1, isRxNotEmpty);
//     return @truncate(u8, regs.SPI1.DR.read_raw());
// }

// Received bytes will overwrite byte array starting at bytes[1]
// First byte does not get overwritten because no full-duplex for first byte
pub fn sendReceive(bytes: []u8) SpiError!void {
    regs.SPI1.CR1.modify(.{ .SPE = 0b1 }); //enable spi
    defer regs.SPI1.CR1.modify(.{ .SPE = 0b0 }); //disable spi
    for (bytes) |byte, index| {
        while (!isTxEmpty()) {}
        regs.SPI1.DR.write_raw(@intCast(u32, byte));
        if (index != 0) {
            while (!isRxNotEmpty()) {}
            bytes[index - 1] = @truncate(u8, regs.SPI1.DR.read_raw());
        }
    }

    while (!isRxNotEmpty()) {}
    bytes[bytes.len - 1] = @truncate(u8, regs.SPI1.DR.read_raw());

    try withTimeout(10, 1, isTxEmpty);
    try withTimeout(20, 5, isNotBusy);
}

inline fn isTxEmpty() bool {
    return regs.SPI1.SR.read().TXE == 0b1;
}

inline fn isRxNotEmpty() bool {
    return regs.SPI1.SR.read().RXNE == 0b1;
}

inline fn isNotBusy() bool {
    return regs.SPI1.SR.read().BSY == 0b0;
}

fn withTimeout(limit_us: u32, step_us: u32, func: fn () callconv(.Inline) bool) SpiError!void {
    var count: u32 = 0;
    while (count < limit_us) {
        if (func()) return;
        count += step_us;
        delay_us(step_us);
    }
    return SpiError.Timeout;
}
