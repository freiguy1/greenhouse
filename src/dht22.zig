const std = @import("std");
const regs = @import("regs.zig");
const delay_us = @import("delay.zig").delay_us;

// Hooked up to PA9
const TempHumidity = struct { temp: f32, humidity: f32 };

const Error = error{Timeout};

// assumes pin is already configured as open drain output
pub fn read() Error!TempHumidity {
    // Set pin to low for ~1ms
    regs.GPIOA.CRH.modify(.{ .MODE9 = 0b10, .CNF9 = 0b00 }); // Output push-pull
    regs.GPIOA.ODR.modify(.{ .ODR9 = 0 }); // pull/ground
    delay_us(1_100); // 1.1ms

    regs.GPIOA.CRH.modify(.{ .MODE9 = 0b00, .CNF9 = 0b10 }); // Input with pull up/down
    regs.GPIOA.ODR.modify(.{ .ODR9 = 0b1 }); // Pull up (high impedence)
    delay_us(55); // get past 20-40us initialization time

    try delayWhileLow(100); // should be 80us
    try delayWhileHigh(100); // should be 80us

    var bits_read: u6 = 0;
    var bytes: [5]u8 = [_]u8{0} ** 5;
    while (bits_read < 40) {
        // Wait through inital 50us low pulse
        try delayWhileLow(70);

        // Wait until short pulse done, but long pulse still high
        delay_us(35);
        if (regs.GPIOA.IDR.read().IDR9 == 0b1) {
            // long pulse is 1
            const byte_index = bits_read / 8;
            const bit_index = 7 - bits_read % 8;
            bytes[byte_index] |= @as(u8, 1) << @intCast(u3, bit_index);

            // let rest of 1 pulse finish before looping
            try delayWhileHigh(45); // 70us - 35us = 35us
        }

        bits_read += 1;
    }

    const humidity = @intToFloat(f32, (@intCast(u16, bytes[0]) << 8) + @intCast(u16, bytes[1])) / 10.0;
    var temp = @intToFloat(f32, (@intCast(u16, bytes[2] & 0x7F) << 8) + @intCast(u16, bytes[3])) / 10.0;
    if (bytes[2] & 0x80 != 0) {
        temp = temp * -1.0;
    }

    temp = temp * 1.8 + 32.0;

    return TempHumidity{ .temp = temp, .humidity = humidity };
}

fn delayWhileHigh(limit_us: u32) Error!void {
    var count: u32 = 0;
    while (count < limit_us) {
        if (regs.GPIOA.IDR.read().IDR9 == 0) return;
        count += 1;
        delay_us(1);
    }
    return error.Timeout;
}

fn delayWhileLow(limit_us: u32) Error!void {
    var count: u32 = 0;
    while (count < limit_us) {
        if (regs.GPIOA.IDR.read().IDR9 == 1) return;
        count += 1;
        delay_us(1);
    }
    return error.Timeout;
}
