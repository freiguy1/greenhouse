const std = @import("std");
const spi = @import("spi.zig");
const delay_us = @import("delay.zig").delay_us;
const delay_ms = @import("delay.zig").delay_ms;
const regs = @import("rf/regs.zig");
const sys_regs = @import("regs.zig");
const consts = @import("consts.zig");

pub const Mode = enum(u3) {
    Sleep = 0b000,
    Standby = 0b001,
    FrequencySynthesizer = 0b010,
    Transmitter = 0b011,
    Receiver = 0b100,
};

// Assumes GPIOA is turned on
pub fn init_pins() void {
    sys_regs.GPIOA.CRL.modify(.{ .MODE3 = 0b10, .CNF3 = 0b00 }); // PA3 = output 2MHz (rfm69 RESET)
    sys_regs.GPIOA.ODR.modify(.{ .ODR3 = 0b0 }); // PA3 pull down: don't reset
    spi.init();
}

pub fn reset_and_init_settings() spi.SpiError!void {
    sys_regs.GPIOA.ODR.modify(.{ .ODR3 = 0b1 }); // Pull reset pin up
    delay_us(100);
    sys_regs.GPIOA.ODR.modify(.{ .ODR3 = 0b0 }); // Pull reset pin back down
    delay_ms(5);

    // set fixed length to 64 bytes
    try writeByte(regs.PACKET_CONFIG1, 0b00010010); //fixed, crc, use node address
    try writeByte(regs.PAYLOAD_LENGTH, 64);

    // set up network id (sync) EDF ;)
    try writeByte(regs.SYNC_CONFIG, 0b10010000); // syncSize to 2 (which is 3 bytes, since 0 based)
    try writeByte(regs.SYNC_VALUE1, 0xEE);
    try writeByte(regs.SYNC_VALUE2, 0xDD);
    try writeByte(regs.SYNC_VALUE3, 0xFF);

    // set up this node's address
    try writeByte(regs.NODE_ADDRS, consts.my_addr);

    try writeByte(regs.FIFO_THRESH, 0x8F); // tx start condition fifo not empty

    // set up frequency for 433
    try writeByte(regs.FRF_MSB, 0x6C);
    try writeByte(regs.FRF_MID, 0x40);
    try writeByte(regs.FRF_LSB, 0x00);

    // // set up bit rate 38.4k
    // try writeByte(regs.BITRATE_MSB, 0x03);
    // try writeByte(regs.BITRATE_LSB, 0x41);

    // // ramp up the frequency deviation 40k (40,000/61 = 0x0290)
    // try writeByte(regs.FDEV_MSB, 0x02);
    // try writeByte(regs.FDEV_MSB, 0x90);

    // // set bitrate ------- DEFAULT
    // try writeByte(regs.BITRATE_MSB, 0x1a);
    // try writeByte(regs.BITRATE_LSB, 0x0b);

    // // set the frequency deviation ----- DEFAULT
    // try writeByte(regs.FDEV_MSB, 0x00);
    // try writeByte(regs.FDEV_MSB, 0x52);

    // set the frequency deviation ----- double default
    // try writeByte(regs.FDEV_MSB, 0x00);
    // try writeByte(regs.FDEV_MSB, 0xA4);
}

pub fn get_temp() spi.SpiError!i8 {
    const TEMP_MEAS_START = 0x08;
    const TEMP_MEAS_RUNNING = 0x04;

    // Start taking temperature
    try writeByte(regs.TEMP1, TEMP_MEAS_START);

    // Delay until temp measurement not running
    var temp_bytes: [2]u8 = undefined;
    while (true) {
        delay_us(20);
        temp_bytes = try readBytes(regs.TEMP1, 2);
        if (temp_bytes[0] & TEMP_MEAS_RUNNING == 0) {
            break;
        }
    }

    // 162ish seems to be a magic number for this particular board
    return @intCast(i8, 162 - @intCast(i9, temp_bytes[1]));
}

/// Send bytes to another RFM69. This blocks until the packet is sent (potentially tens of milliseconds)
pub fn send(comptime length: u8, buffer: [length]u8) spi.SpiError!void {
    const timeout: u8 = std.math.maxInt(u8);
    var counter: u8 = 0;

    try setMode(.Standby);
    while (counter <= timeout) : (counter += 1) {
        delay_us(20);
        if (try isModeReady()) break;
    } else {
        return spi.SpiError.Timeout;
    }
    counter = 0;

    try resetFifo();
    try writeBytes(regs.FIFO, length, buffer);

    // Set high power
    try writeByte(regs.PA_LEVEL, 0x7F); // Use PA1 and PA2 (not PA0)
    try writeByte(regs.TEST_PA1, 0x5D); // set to magic high power value
    try writeByte(regs.TEST_PA2, 0x7C); // set to magic high power value
    try writeByte(regs.OCP, 0x0A); // disable over current protection, required for HCW variant, i guess?
    defer {
        // reduce power
        writeByte(regs.TEST_PA1, 0x55) catch {};
        writeByte(regs.TEST_PA2, 0x70) catch {};
        writeByte(regs.PA_LEVEL, 0x9F) catch {}; // Use PA0
        writeByte(regs.OCP, 0x1A) catch {}; // enable over current protection
    }

    try setMode(.Transmitter);
    defer setMode(.Standby) catch {};

    // After nights of stalling, I learned that transmission is much slower
    // than originally thought. 5 byte message was taking ~23ms at default speed.
    while (counter < timeout) : (counter += 1) {
        delay_us(500);
        if (try isPacketSent()) break;
    } else {
        return spi.SpiError.Timeout;
    }
}

fn resetFifo() spi.SpiError!void {
    try writeByte(regs.IRQ_FLAGS2, 0x10);
}

fn setMode(mode: Mode) spi.SpiError!void {
    const value = @intCast(u8, @enumToInt(mode)) << 2;
    try writeByte(regs.OP_MODE, value);
}

fn isModeReady() spi.SpiError!bool {
    const irq_flags = try readByte(regs.IRQ_FLAGS1);
    return irq_flags & 0x80 != 0;
}

fn isPacketSent() spi.SpiError!bool {
    const irq_flags = try readByte(regs.IRQ_FLAGS2);
    return irq_flags & 0x08 != 0;
}

fn writeByte(addr: u7, value: u8) spi.SpiError!void {
    try writeBytes(addr, 1, [_]u8{value});
}

fn writeBytes(addr: u7, comptime length: u8, values: [length]u8) spi.SpiError!void {
    var buf: [length + 1]u8 = undefined;
    buf[0] = @intCast(u8, addr) | 0x80;
    std.mem.copy(u8, buf[1..], &values);
    try spi.sendReceive(&buf);
}

fn readByte(addr: u7) spi.SpiError!u8 {
    return (try readBytes(addr, 1))[0];
}

fn readBytes(addr: u7, comptime length: u8) spi.SpiError![length]u8 {
    var buf: [length + 1]u8 = [_]u8{0} ** (length + 1);
    buf[0] = @intCast(u8, addr);
    try spi.sendReceive(&buf);
    return buf[1..].*;
}
